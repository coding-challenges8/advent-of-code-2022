#!/bin/sh

case "$@" in
	'b'|'build')
		podman build --target runner -t runner-aoc22 .
		;;
	'i'|'inspect')
		podman run --rm -t --name oci-aoc22 --entrypoint ash runner-aoc22
		;;
	*)
		podman run --rm \
			--name oci-aoc22 \
			-v "$PWD"/data:/app/data:ro \
			-t runner-aoc22
;;
esac
