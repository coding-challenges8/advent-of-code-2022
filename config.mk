VERSION = 0.1

# based on `pkg-config --cflags glib-2.0`
CFLAGS = -g -pedantic -std=c17 -I /usr/lib/glib-2.0/include -I \
		 /usr/include/glib-2.0 -I /usr/include/sysprof-4 -pthread

# TODO: Support static builds with GLib.
# LDFLAGS = -static

# based on `pkg-config --libs glib-2.0`
LDLIBS = -l glib-2.0 

LINT = splint
LINTFLAGS = +posixlib

UNITS = aoc22 part1
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)

CC = cc
