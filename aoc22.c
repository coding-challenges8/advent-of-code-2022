/* Defines the entrypoint for the CLI based program and dispatches to
 * the various consituent prompts to solve. Note that the various compilation
 * units can be build as standalone programs. */
#include <glib.h>

#include "day1.h"

#define CASE_DAY(x) \
    case x: \
    if (1 == part) { d##x##p1(); } else if (2 == part) { d##x##p2(); } \
    else { g_warning("A valid part was not chosen."); return 2; } \
    break

int
main(int argc, char * argv[]) {
    gchar const * description = "This program may be run with various \
                                 arguments solve the various problems \
                                 featured in Advent of Code 2022.";
    gchar const * summary = "Solve AoC 2022 problems.";
    gint day = 0;
    gint part = 0;
    gchar * in_fname = NULL;
    GOptionEntry const entries[] = {
        {"day", 'd', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &day,
            "which day's problem", NULL },
        {"part", 'p', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &part,
            "which part of the problem (1,2)", NULL},
        {"infile", 'i', G_OPTION_FLAG_NONE, G_OPTION_ARG_FILENAME, &in_fname,
            "file name of problem dataset", NULL},
        {NULL}
    };
    GError *error = NULL;
    GOptionContext *context = g_option_context_new(" - solve Advent of Code 2022's problems.");
    g_option_context_set_description(context, description);
    g_option_context_set_summary(context, summary);
    g_option_context_add_main_entries(context, entries, NULL);
    g_option_context_parse(context, &argc, &argv, &error);
    switch (day) {
        CASE_DAY(1);
        /*
        CASE_DAY(2);
        ...
         */
        default:
            g_warning("A valid day was not selected.");
            return 1;
    }
    g_option_context_free(context);
}
