#include <stdio.h>
#include <stdlib.h>
#define G_LOG_DOMAIN "aoc22-part1a"
#define G_LOG_USE_STRUCTURED
#include <glib.h>
#include "day1.h"

#define len_buffer 80

GSList *getPSListMaxCalories();
GSList *storeMaxCalories();

/* TODO: Factor out data common to both parts to outside of both functions,
 * either in some config function that may source data from the overall call
 * or environment, or from the header. */
int
#ifdef STANDALONE_D1P1
main() {
#else
d1p1() {
#endif
    /* Process input. */
	GSList *pSListMaxCalories = getPSListMaxCalories();
    if(pSListMaxCalories) {
	    /* RFE: Change message based on log level. */
	    g_info("The maximum caloric subtotal is...");
	    g_print("%d\n", GPOINTER_TO_UINT(pSListMaxCalories->data));
    }
	g_slist_free(pSListMaxCalories);
	pSListMaxCalories = NULL;
	return 0;
}

int
#ifdef STANDALONE_D1P2
main() {
#else
d1p2() {
#endif
    /* Process input. */
	GSList *pSListMaxCalories = getPSListMaxCalories();
    if(pSListMaxCalories) {
	    /* RFE: Change message based on log level. */
	    g_info("The top three maximum caloric subtotals combined are...");
        guint calories_triple = 0;
        for (gint i = 0; i < 3 && pSListMaxCalories; ++i, pSListMaxCalories = pSListMaxCalories->next) {
	    	g_debug("calories subtotal(%d) is: %d", i, GPOINTER_TO_UINT(pSListMaxCalories->data));
            calories_triple += GPOINTER_TO_UINT(pSListMaxCalories->data);
	    }
	    g_print("%d\n", calories_triple);
    }
    return 0;
}

int cmpMaxCalories(gconstpointer a, gconstpointer b) {
    g_debug("Entering cmpMaxCalories...");
    g_debug("Casted %d!", GPOINTER_TO_UINT(a));
    return GPOINTER_TO_UINT(a) < GPOINTER_TO_UINT(b);
}

/* Loads a SList of the elves' caloric subtotals from greatest to least. */
GSList *getPSListMaxCalories() {
	guint calories = 0;
	guint calories_subtotal = 0;
	gchar *buffer = g_new(gchar, len_buffer);
    GSList *pSListMaxCalories = NULL;
    /* TODO: There must be a better way to do this. */
    gboolean latch_input = FALSE; /* At least one record is read. */
	/* input loop */
	while (fgets(buffer, len_buffer, stdin)) {
		g_debug("buffer: %s", buffer);
		if (calories = strtol(buffer, NULL, 10)) {
			/* Found a valid number. Add it to this elf's subtotal. */
            latch_input = TRUE;
			calories_subtotal += calories;
			g_debug("calories: %d", calories);
		} else if (0 == g_strcmp0(buffer, "\n")) {
			/* Found a new line. Save the current value off. */
            pSListMaxCalories = storeMaxCalories(pSListMaxCalories, calories_subtotal);
			/* Prepare to count the next elf's caloric subtotal total. */
			calories_subtotal = 0;
		}
        g_debug("buffer remaining is: %s", buffer);
	}
    /* There is a final record to process, but no newline to trigger it. */
    if (latch_input) {
        pSListMaxCalories = storeMaxCalories(pSListMaxCalories, calories_subtotal);
    }
    g_free(buffer);
    return pSListMaxCalories;
}

GSList *storeMaxCalories(GSList *pSListMaxCalories, guint calories_subtotal) {
    g_debug("calories_subtotal: %d", calories_subtotal);
    gpointer pCaloriesSubtotal = GUINT_TO_POINTER(calories_subtotal);
    if (!pSListMaxCalories) {
        g_debug("Prepending %d", calories_subtotal);
        pSListMaxCalories = g_slist_prepend(pSListMaxCalories, pCaloriesSubtotal);
    } else {
        g_debug("Sorting %d", calories_subtotal);
        pSListMaxCalories = g_slist_insert_sorted(pSListMaxCalories, pCaloriesSubtotal, &cmpMaxCalories);
    }
    return pSListMaxCalories;
}
