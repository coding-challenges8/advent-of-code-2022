.PHONY: all check clean debug dump
.POSIX:

include config.mk

all : aoc22

aoc22 : day1.o
aoc22.o : day1.h
day1.o : day1.h

debug : day1

# This will be used until GLib logging/GTest can be implemented.
# It would be slick if `cat` could be eliminated through redirection.
check : aoc22
	diff <(./$< -d 1 -p 1 < data/sample1.txt) data/answer1.txt \
		&& printf "Success!\n" \
		|| printf "Failure!\n"

clean :
	$(RM) aoc22.log aoc22 $(OBJ)

dump:
	$(info CFLAGS="$(CFLAGS)")
	$(info LDFLAGS="$(LDFLAGS)")
	$(info LINT="$(LINT)")
	$(info LINTFLAGS="$(LINTFLAGS)")
	$(info SRC="$(SRC)")
	$(info OBJ="$(OBJ)")

# standalone compilation
# TODO: Pattern match this so all progs can be built stand-alone.
day1 : day1.c
	$(CC) -D X $(CPPFLAGS) $(CFLAGS) part1.c $(LDLIBS) -o part1
